package com.task.angelina.networkapplication.network.service;

import com.task.angelina.networkapplication.models.changes.Information;
import com.task.angelina.networkapplication.utils.Constants;
import io.reactivex.Single;
import java.util.Map;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ChangeDetailsService {
    @GET(Constants.QUERY_PARAM)
    Single<Information> getChangeInformation(@QueryMap Map<String, String> queries);
}
