package com.task.angelina.networkapplication.mvp.companylist;

public interface CompanyListPresenter {
    void loadNext();

    void refresh();

    void openDetailsFragment(String ticker);

    void onBackPressed();
}
