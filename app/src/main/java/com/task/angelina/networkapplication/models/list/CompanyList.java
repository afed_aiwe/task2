package com.task.angelina.networkapplication.models.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class CompanyList {
    @SerializedName("companies")
    @Expose
    private List<CompanyInfo> companies = null;
    @SerializedName("next_page")
    @Expose
    private String nextPage;

    public CompanyList(List<CompanyInfo> companies, String nextPage) {
        this.companies = companies;
        this.nextPage = nextPage;
    }

    public List<CompanyInfo> getCompanies() {
        return companies;
    }

    public String getNextPage() {
        return nextPage;
    }
}
