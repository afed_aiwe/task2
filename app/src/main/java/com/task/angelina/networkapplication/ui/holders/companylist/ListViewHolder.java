package com.task.angelina.networkapplication.ui.holders.companylist;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import com.task.angelina.networkapplication.R;
import com.task.angelina.networkapplication.models.list.CompanyInfo;
import com.task.angelina.networkapplication.models.single.CompanyProfile;
import com.task.angelina.networkapplication.utils.StringUtils;
import java.util.Formatter;

public class ListViewHolder extends RecyclerView.ViewHolder {
    private ImageView imgCompany;
    private TextView txtName;
    private TextView txtTicker;
    private TextView txtPrice;
    private TextView txtChanged;
    private Context context;

    public ListViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        this.context = context;
        imgCompany = itemView.findViewById(R.id.img_company);
        txtName = itemView.findViewById(R.id.txt_company_name);
        txtTicker = itemView.findViewById(R.id.txt_ticker);
        txtPrice = itemView.findViewById(R.id.txt_price);
        txtChanged = itemView.findViewById(R.id.txt_changed);
    }

    public void bindCompanyInfo(CompanyInfo companyInfo) {
        StringBuilder builder;

        //companyInfo maybe null
        if (companyInfo != null) {
            txtName.setText(StringUtils.checkString(companyInfo.getName()));
            txtTicker.setText(StringUtils.checkString(companyInfo.getTicker()));

            //companyProfile maybe null
            CompanyProfile companyProfile = companyInfo.getCompanyProfile();
            if (companyProfile != null) {

                txtPrice.setText(String.format("%.2f", StringUtils.checkDouble(companyProfile.getPrice())));

                imgCompany.setVisibility(View.VISIBLE);
                Picasso.with(context).load(StringUtils.checkString(companyProfile.getImage()))
                        .into(imgCompany);

                builder = new StringBuilder();
                Formatter formatter = new Formatter(builder);
                formatter.format("%.2f %s", StringUtils.checkDouble(companyProfile.getChanges()),
                        StringUtils.checkString(companyProfile.getChangesPercentage()));
                txtChanged.setText(builder.toString());

                if (StringUtils.checkDouble(companyProfile.getChanges()) >= 0) {
                    txtChanged.setTextColor(ContextCompat.getColor(context, R.color.colorAccentPlus));
                } else {
                    txtChanged.setTextColor(ContextCompat.getColor(context, R.color.colorAccentMinus));
                }
            } else {
                setDefaultValue();
            }
        } else {
            txtName.setText(R.string.not_available);
            txtTicker.setText(R.string.not_available);
            setDefaultValue();
        }
    }

    private void setDefaultValue() {
        txtPrice.setText(R.string.not_available);
        txtChanged.setText(R.string.not_available);
        imgCompany.setVisibility(View.GONE);
    }
}
