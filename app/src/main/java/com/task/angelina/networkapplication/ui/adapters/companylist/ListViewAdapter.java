package com.task.angelina.networkapplication.ui.adapters.companylist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.task.angelina.networkapplication.R;
import com.task.angelina.networkapplication.models.list.CompanyInfo;
import com.task.angelina.networkapplication.ui.holders.companylist.ListViewHolder;
import com.task.angelina.networkapplication.ui.holders.companylist.ProgressHolder;
import java.util.ArrayList;
import java.util.List;

public class ListViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int NORMAL = 0;
    private static final int LOADING = 1;
    private List<CompanyInfo> companyInfoList;
    private boolean isLoaderVisible = false;
    private ItemOnClickListener listener;

    public ListViewAdapter(ItemOnClickListener listener) {

        this.companyInfoList = new ArrayList<>();
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case NORMAL:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_company, parent, false);
                viewHolder = new ListViewHolder(view, parent.getContext());
                ViewHolder finalViewHolder = viewHolder;
                view.setOnClickListener(v -> {
                    int position = finalViewHolder.getAdapterPosition();
                    final CompanyInfo companyInfo = companyInfoList.get(position);
                    listener.onClickItem(companyInfo.getTicker());
                });
                break;
            case LOADING:
                viewHolder = new ProgressHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.loading,parent,false));
                break;
               default:
                   break;
        }
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case NORMAL:
                final ListViewHolder listViewHolder = (ListViewHolder) holder;
                final CompanyInfo companyInfo = companyInfoList.get(position);
                listViewHolder.bindCompanyInfo(companyInfo);
                break;
            case LOADING:
                break;
        }

    }

    public List<CompanyInfo> getList() {
        return companyInfoList;
    }
    @Override
    public int getItemCount() {

        int itemsCount = companyInfoList != null ? companyInfoList.size() : 0;
        return isLoaderVisible ? itemsCount + 1 : itemsCount;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == companyInfoList.size()) {
            return LOADING;
        }
        return NORMAL;
    }

    public void add(CompanyInfo companyInfo) {
        companyInfoList.add(companyInfo);
        notifyItemInserted(companyInfoList.size());
    }

    public void addAll(List<CompanyInfo> companyInfoList) {
        if (companyInfoList != null) {
            for (int i = 0; i < companyInfoList.size(); i++) {
                add(companyInfoList.get(i));
            }
        }
    }

    public void remove(CompanyInfo companyInfo) {
        int position = companyInfoList.indexOf(companyInfo);
        if (position > -1) {
            companyInfoList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoaderVisible = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void setHasMoreItems(boolean isLoaderVisible) {
        this.isLoaderVisible = isLoaderVisible;
    }

    private CompanyInfo getItem(int i) {
        return companyInfoList.get(i);
    }

    public interface ItemOnClickListener {
        void onClickItem(String ticker);
    }
}
