package com.task.angelina.networkapplication.mvp.companydetails;

import com.task.angelina.networkapplication.base.BaseView;
import com.task.angelina.networkapplication.models.single.CompanyDetail;
import java.util.LinkedHashMap;
import java.util.List;

public interface CompanyDetailsView extends BaseView {
    void companyLoadedSuccess(CompanyDetail companyDetail);

    void companyLoadedError(String error);

    void stockPeriodLoadedSuccess(List<Double> result);

    void stockOptionLoadedSuccess(LinkedHashMap<String, Float> result);

    void progressBarVisibility(boolean isPeriod,int visibility);
}
