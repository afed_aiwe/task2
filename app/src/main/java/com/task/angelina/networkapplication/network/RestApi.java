package com.task.angelina.networkapplication.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.task.angelina.networkapplication.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestApi {
    private static Retrofit retrofitAll = null;
    private static Retrofit retrofitDetail = null;
    private static Retrofit retrofitChange = null;

    public static void initChange() { retrofitChange = provideRetrofit(Constants.CHANGE_COMPANY_URL);}

    public static void initAll() {
        retrofitAll = provideRetrofit(Constants.ALL_COMPANIES_URL);
    }

    public static void init() {
        retrofitDetail = provideRetrofit(Constants.DETAIL_COMPANY_URL);
    }

    public static <T> T createServiceAll(Class<T> serviceClass) {
        if (retrofitAll == null) {
            throw new IllegalStateException("Call `RestApi.initAll()` before calling this method.");
        }
        return retrofitAll.create(serviceClass);

    }

    public static <T> T createServiceDetail(Class<T> serviceClass) {
        if (retrofitDetail == null) {
            throw new IllegalStateException("Call `RestApi.init()` before calling this method.");
        }
        return retrofitDetail.create(serviceClass);
    }

    public static <T> T createServiceChange(Class<T> serviceClass) {
        if (retrofitChange == null) {
            throw new IllegalStateException("Call `RestApi.iniChange()` before calling this method.");
        } else {
            return retrofitChange.create(serviceClass);
        }
    }

    private static Retrofit provideRetrofit(String baseUrl) {
        Gson gson = new GsonBuilder().setLenient().create();
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(provideOkHttpClient())
                .build();
    }

    private static OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
    }
}
