package com.task.angelina.networkapplication.utils;

import androidx.annotation.Nullable;

public class StringUtils {
    public static String checkString(@Nullable String value) {
        return value == null ? "" : value;
    }

    public static Double checkDouble(@Nullable Double value) {
        return value == null ? 0.0 : value;
    }
}
