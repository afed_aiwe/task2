package com.task.angelina.networkapplication.network.service;

import com.task.angelina.networkapplication.models.list.CompanyList;
import com.task.angelina.networkapplication.utils.Constants;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AllCompaniesService {
    @GET(Constants.GET_COMPANIES_POINT)
    Single<CompanyList> getAllCompanies(@Query(Constants.HEADER_API_LIST_PARAM) String apiKey,
                                            @Query(Constants.NEXT_PAGE_PARAM) String nextPage,
                                            @Query(Constants.PAGE_SIZE_PARAM) Integer pageSize);
}
