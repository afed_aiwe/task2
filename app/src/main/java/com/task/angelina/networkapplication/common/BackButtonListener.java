package com.task.angelina.networkapplication.common;

public interface BackButtonListener {

    boolean onBackPressed();

}
