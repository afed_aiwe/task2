package com.task.angelina.networkapplication.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.task.angelina.networkapplication.R;
import com.task.angelina.networkapplication.base.BaseFragment;
import com.task.angelina.networkapplication.common.BackButtonListener;
import com.task.angelina.networkapplication.models.list.CompanyInfo;
import com.task.angelina.networkapplication.mvp.companylist.CompanyListPresenterImpl;
import com.task.angelina.networkapplication.mvp.companylist.CompanyListView;
import com.task.angelina.networkapplication.ui.adapters.companylist.ListViewAdapter;
import com.task.angelina.networkapplication.utils.PaginationScrollListener;
import java.util.List;

public class CompanyListFragment extends BaseFragment implements CompanyListView, SwipeRefreshLayout.OnRefreshListener, BackButtonListener {
    private CompanyListPresenterImpl companyListPresenter;
    private ListViewAdapter listViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    public CompanyListFragment() {
    }

    public static CompanyListFragment newInstance() {
        CompanyListFragment fragment = new CompanyListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private ListViewAdapter.ItemOnClickListener itemOnClickListener = ticker -> {
        companyListPresenter.openDetailsFragment(ticker);
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_company_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.rv_company);
        refreshLayout = view.findViewById(R.id.swipe);
        refreshLayout.setOnRefreshListener(this);

        listViewAdapter = new ListViewAdapter(itemOnClickListener);

        linearLayoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(listViewAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreCompany() {
                companyListPresenter.loadNext();
            }
        });

        companyListPresenter.refresh();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        companyListPresenter = new CompanyListPresenterImpl();
        companyListPresenter.setView(this);
    }

    @Override
    public void onDetach() {
        companyListPresenter.onDestroy();

        super.onDetach();
    }

    @Override
    public void onRefresh() {
        companyListPresenter.refresh();
    }

    @Override
    public void setHasMoreItems(boolean hasMoreItems) {
        listViewAdapter.setHasMoreItems(hasMoreItems);
    }

    @Override
    public void onPageLoadedSuccess(List<CompanyInfo> companyInfoList, boolean isFirstPage) {
            if (isFirstPage) {
                listViewAdapter.clear();
            }
            listViewAdapter.addAll(companyInfoList);
    }

    @Override
    public void onPageLoadedError(String error) {
        showAlertDialog(error).show();
    }

    @Override
    public boolean onBackPressed() {
        companyListPresenter.onBackPressed();
        return true;
    }
}
