package com.task.angelina.networkapplication.ui.holders.companydetails;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.task.angelina.networkapplication.R;

public class DetailsPeriodHolder extends RecyclerView.ViewHolder {
    private TextView txtTitlePeriod;
    private TextView txtChangePeriod;
    private Context context;

    public DetailsPeriodHolder(@NonNull View itemView, Context context) {
        super(itemView);

        this.context = context;
        txtTitlePeriod = itemView.findViewById(R.id.txt_title_period);
        txtChangePeriod =itemView.findViewById(R.id.txt_change_period);
    }

    public void bindChangeInfo(Double result, int position) {
        txtTitlePeriod.setText(context.getResources().getStringArray(R.array.options)[position]);
        txtChangePeriod.setTextColor(result >= 0 ? context.getColor(R.color.colorAccentPlus) : context.getColor(R.color.colorAccentMinus));
        txtChangePeriod.setText(String.format("%.2f",result));
    }
}
