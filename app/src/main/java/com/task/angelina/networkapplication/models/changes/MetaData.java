package com.task.angelina.networkapplication.models.changes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MetaData {
    @SerializedName("1. Information")
    @Expose
    private String information;
    @SerializedName("2. Symbol")
    @Expose
    private String symbol;
    @SerializedName("3. Last Refreshed")
    @Expose
    private String lastRefreshed;
    @SerializedName("4. Interval")
    @Expose
    private String interval;

    public String getInformation() {
        return information;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getLastRefreshed() {
        return lastRefreshed;
    }

    public String getInterval() {
        return interval;
    }
}
