package com.task.angelina.networkapplication.base;

import android.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.task.angelina.networkapplication.R;

public abstract class BaseFragment extends Fragment implements BaseView {

    protected SwipeRefreshLayout refreshLayout;

    @Override
    public AlertDialog showAlertDialog(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.getContext());
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle(R.string.dialog_title);
        alertDialogBuilder.setMessage(this.getResources().getString(R.string.network_status) + message);
        alertDialogBuilder.setNeutralButton(R.string.dialog_neutral_button, (dialog, which) -> dialog.cancel());
        AlertDialog alertDialog = alertDialogBuilder.create();
        return alertDialog;
    }

    @Override
    public void changeRefreshLoadingVisibility(boolean isVisible) {
        refreshLayout.setRefreshing(isVisible);
    }
}
