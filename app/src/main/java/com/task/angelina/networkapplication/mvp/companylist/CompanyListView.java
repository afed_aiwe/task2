package com.task.angelina.networkapplication.mvp.companylist;

import com.task.angelina.networkapplication.base.BaseView;
import com.task.angelina.networkapplication.models.list.CompanyInfo;
import java.util.List;

public interface CompanyListView extends BaseView {
    void setHasMoreItems(boolean hasMoreItems);

    void onPageLoadedSuccess(List<CompanyInfo> companyInfoList, boolean isFirstPage);

    void onPageLoadedError(String error);
}
