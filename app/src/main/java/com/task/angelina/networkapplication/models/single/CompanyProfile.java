package com.task.angelina.networkapplication.models.single;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyProfile {
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("changes")
    @Expose
    private Double changes;
    @SerializedName("changesPercentage")
    @Expose
    private String changesPercentage;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;

    public Double getPrice() {
        return price;
    }

    public Double getChanges() {
        return changes;
    }

    public String getChangesPercentage() {
        return changesPercentage;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
