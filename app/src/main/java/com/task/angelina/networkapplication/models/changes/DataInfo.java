package com.task.angelina.networkapplication.models.changes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataInfo {
    @SerializedName("1. open")
    @Expose
    private String open;
    @SerializedName("2. high")
    @Expose
    private String high;
    @SerializedName("3. low")
    @Expose
    private String low;
    @SerializedName("4. close")
    @Expose
    private String close;
    @SerializedName("5. volume")
    @Expose
    private String volume;

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getHigh() {
        return high;
    }

    public String getLow() {
        return low;
    }

    public String getClose() {
        return close;
    }

    public String getVolume() {
        return volume;
    }
}
