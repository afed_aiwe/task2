package com.task.angelina.networkapplication.ui.adapters.companydetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.task.angelina.networkapplication.R;

public class SpinnerAdapter extends BaseAdapter {
    private String[] period;
    private LayoutInflater layoutInflater;

    public SpinnerAdapter(Context context, String[] period) {
        this.period = period;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return period.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = layoutInflater.inflate(R.layout.spinner_time_layout, null);
        TextView txtFunction = view.findViewById(R.id.txt_function);
        txtFunction.setText(period[position]);
        return view;
    }
}
