package com.task.angelina.networkapplication.ui.adapters.companydetails;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.task.angelina.networkapplication.R;
import com.task.angelina.networkapplication.ui.holders.companydetails.DetailsPeriodHolder;
import java.util.ArrayList;
import java.util.List;

public class DetailsPeriodAdapter extends RecyclerView.Adapter<DetailsPeriodHolder> {
    private List<Double> results = new ArrayList<>();

    public DetailsPeriodAdapter() {
    }

    public void setResultArray(List<Double> resultArray) {
        results = resultArray;
    }

    @NonNull
    @Override
    public DetailsPeriodHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_change_param, parent, false);
        return new DetailsPeriodHolder(view, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull DetailsPeriodHolder holder, int position) {
        final Double res = results.get(position);
        holder.bindChangeInfo(res, position);
    }

    @Override
    public int getItemCount() {
        return results != null ? results.size() : 0;
    }
}
