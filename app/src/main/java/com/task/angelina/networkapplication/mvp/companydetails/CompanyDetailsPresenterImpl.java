package com.task.angelina.networkapplication.mvp.companydetails;

import android.util.Log;
import android.view.View;
import com.task.angelina.networkapplication.NetworkApp;
import com.task.angelina.networkapplication.base.BasePresenter;
import com.task.angelina.networkapplication.models.changes.DataInfo;
import com.task.angelina.networkapplication.models.changes.Information;
import com.task.angelina.networkapplication.network.RestApi;
import com.task.angelina.networkapplication.network.service.ChangeDetailsService;
import com.task.angelina.networkapplication.network.service.SingleCompanyService;
import com.task.angelina.networkapplication.utils.Constants;
import com.task.angelina.networkapplication.utils.NumberUtils;
import com.task.angelina.networkapplication.utils.OptionOfChanges;
import com.task.angelina.networkapplication.utils.PeriodOfChanges;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import ru.terrakok.cicerone.Router;

public class CompanyDetailsPresenterImpl extends BasePresenter<CompanyDetailsView> implements CompanyDetailsPresenter {

    private static final String TAG = CompanyDetailsPresenterImpl.class.getName();
    private static final DateTimeFormatter DTF = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter DF = DateTimeFormat.forPattern("yyyy-MM-dd");
    private static final String REG_DEF = "-";
    private static final String REG_POINT = ":";

    private SingleCompanyService singleCompanyService;
    private Router router;
    private ChangeDetailsService changeDetailsService;
    private CompositeDisposable compositeDisposable;
    private LocalDate localDate;
    private PeriodOfChanges per;
    private OptionOfChanges option;
    private boolean isPeriod = true;

    public CompanyDetailsPresenterImpl() {
        router = NetworkApp.INSTANCE.getRouter();
        singleCompanyService = RestApi.createServiceDetail(SingleCompanyService.class);
        changeDetailsService = RestApi.createServiceChange(ChangeDetailsService.class);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void refresh(String ticker) {
        view.changeRefreshLoadingVisibility(true);
        loadingCompanyDetails(ticker);
    }

    @Override
    public void loadStockPeriodData(String ticker, PeriodOfChanges periodOfChanges) {
        isPeriod = true;
        view.progressBarVisibility(isPeriod, View.VISIBLE);
        // clear items
        view.stockPeriodLoadedSuccess(new ArrayList<>());
        loadingStockData(getQueryParams(ticker, periodOfChanges));
    }

    @Override
    public void loadStockOptionData(String ticker, PeriodOfChanges period, OptionOfChanges option) {
        isPeriod = false;
        this.option = option;
        view.progressBarVisibility(isPeriod, View.VISIBLE);
        loadingStockData(getQueryParams(ticker, period));
    }

    @Override
    public void backPressed() {
        router.exit();
    }

    @Override
    public void onDestroy() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    private Map<String, String> getQueryParams(String ticker, PeriodOfChanges periodOfChanges) {
        Map<String, String> queryParams = new LinkedHashMap<>();
        String periodOfTime = "";
        per = periodOfChanges;
        queryParams.put(Constants.SYMBOL, ticker);
        queryParams.put(Constants.OUT_PUT_SIZE, Constants.FULL_SIZE);
        queryParams.put(Constants.HEADER_API_CHANGE_PARAM, Constants.API_KEY_CHANGE);
        switch (periodOfChanges) {
            case DAY:
                queryParams.put(Constants.FUNCTION_PARAM, Constants.FUNCTION_INTRADAY);
                periodOfTime = Constants.INTERVAL_PARAM;
                break;
            case MONTH:
                queryParams.put(Constants.FUNCTION_PARAM, Constants.FUNCTION_WEEKLY);
                break;
            case HALF_YEAR:
            case YEAR:
                queryParams.put(Constants.FUNCTION_PARAM, Constants.FUNCTION_MONTH);
                break;
        }
        queryParams.put(Constants.INTERVAL_HEADER, periodOfTime);
        return queryParams;
    }

    private void loadingCompanyDetails(String ticker) {
        compositeDisposable.add(singleCompanyService.getCompany(ticker)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> view.changeRefreshLoadingVisibility(false))
                .toObservable()
                .subscribe(companyDetail -> {
                    view.companyLoadedSuccess(companyDetail);
                }, throwable -> {
                    view.companyLoadedError(throwable.getMessage());
                    throwable.printStackTrace();
                }));
    }

    private void loadingStockData(Map<String, String> queries) {
        compositeDisposable.add(changeDetailsService.getChangeInformation(queries)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::accept, throwable -> {
                    view.companyLoadedError(throwable.getMessage());
                    throwable.printStackTrace();
                }));
    }

    private void calculateChange(Map<String, DataInfo> dataInfoMap) {
        List<Double> res = new ArrayList<>();
        final Set<Entry<String, DataInfo>> tempEntry = dataInfoMap.entrySet();
        final int tempEntryLength = tempEntry.size();
        if (tempEntryLength > 0) {
            final Entry<String, DataInfo>[] entry = new Entry[tempEntryLength];
            tempEntry.toArray(entry);
            final DataInfo dataInfoFirst = entry[0].getValue();
            final DataInfo dataInfoLast = entry[tempEntryLength - 1].getValue();

            res.add(NumberUtils.stringToDouble((dataInfoFirst.getOpen())) - NumberUtils
                    .stringToDouble((dataInfoLast.getOpen())));
            res.add(NumberUtils.stringToDouble((dataInfoFirst.getHigh())) - NumberUtils
                    .stringToDouble((dataInfoLast.getHigh())));
            res.add(NumberUtils.stringToDouble((dataInfoFirst.getLow())) - NumberUtils
                    .stringToDouble((dataInfoLast.getLow())));
            res.add(NumberUtils.stringToDouble((dataInfoFirst.getClose())) - NumberUtils
                    .stringToDouble((dataInfoLast.getClose())));
            res.add(NumberUtils.stringToDouble((dataInfoFirst.getVolume())) - NumberUtils
                    .stringToDouble((dataInfoLast.getVolume())));
        } else {
            res = Collections.emptyList();
        }
        view.stockPeriodLoadedSuccess(res);
    }

    private void showOptionChange(Map<String, DataInfo> dataInfoMap) {
        Map<String, Float> res = new LinkedHashMap<>();
        if (dataInfoMap.size() > 0) {
            switch (option) {
                case VOLUME:
                    setKeyInOption(dataInfoMap, (LinkedHashMap<String, Float>) res,
                            dataInfo -> String.valueOf(dataInfo.getVolume()));
                    break;
                case LOW:
                    setKeyInOption(dataInfoMap, (LinkedHashMap<String, Float>) res,
                            dataInfo -> String.valueOf(dataInfo.getLow()));
                    break;
                case CLOSE:
                    setKeyInOption(dataInfoMap, (LinkedHashMap<String, Float>) res,
                            dataInfo -> String.valueOf(dataInfo.getClose()));
                    break;
                case OPEN:
                    setKeyInOption(dataInfoMap, (LinkedHashMap<String, Float>) res,
                            dataInfo -> String.valueOf(dataInfo.getOpen()));
                    break;
                case HIGH:
                    setKeyInOption(dataInfoMap, (LinkedHashMap<String, Float>) res,
                            dataInfo -> String.valueOf(dataInfo.getHigh()));
                    break;
            }
        } else {
            res = Collections.emptyMap();
        }
        view.stockOptionLoadedSuccess((LinkedHashMap<String, Float>) res);
    }

    private void setKeyInOption(Map<String, DataInfo> dataInfoMap,
            LinkedHashMap<String, Float> res,
            GetValueListener listener) {
        for (Map.Entry<String, DataInfo> temp : dataInfoMap.entrySet()) {
            String key;
            if (!per.equals(PeriodOfChanges.DAY)) {
                String[] keys = String.valueOf(temp.getKey()).split(REG_DEF);
                key = keys.length == 3 ? keys[2] + REG_DEF + keys[1] : "";
            } else {
                String[] keys = String.valueOf(temp.getKey()).split(" ")[1].split(REG_POINT);
                key = keys.length == 3 ? keys[0] + REG_POINT + keys[1] : "";
            }
            res.put(key, NumberUtils.stringToFloat(listener.getValue(temp.getValue())));
        }
    }

    private Map<String, DataInfo> getPeriodSeries(Map<String, DataInfo> dataInfoMap, LocalDate date,
            DateTimeFormatter dtf) {
        Map<String, DataInfo> periodSeries = new LinkedHashMap<>();
        for (Map.Entry<String, DataInfo> tempEntry : dataInfoMap.entrySet()) {
            LocalDate local = dtf.parseLocalDate(String.valueOf(tempEntry.getKey()));
            if (local.isAfter(date) || local.isEqual(date)) {
                periodSeries.put(String.valueOf(tempEntry.getKey()), tempEntry.getValue());
            } else {
                return periodSeries;
            }
        }
        return periodSeries;
    }

    private void setLocaleDateTimeFormat(String lastRefreshed, DateTimeFormatter dtf) {
        switch (per) {
            case MONTH:
                localDate = dtf.parseLocalDate(lastRefreshed).minusMonths(1);
                break;
            case HALF_YEAR:
                localDate = dtf.parseLocalDate(lastRefreshed).minusMonths(6);
                break;
            case YEAR:
                localDate = dtf.parseLocalDate(lastRefreshed).minusYears(1);
                break;
            case DAY:
                localDate = dtf.parseLocalDate(lastRefreshed);
                break;
        }
    }

    private void nullObject() {
        view.companyLoadedError(Constants.NOT_FOUND_DATA);
        view.progressBarVisibility(isPeriod, View.GONE);
    }

    private void accept(Information information) {
        if (information.getErrorMessage() != null) {
            nullObject();
            return;
        }
        if (information.getMetaData() == null || information.getMetaData().getLastRefreshed() == null) {
            nullObject();
            return;
        }

        final String lastRefreshed = String.valueOf(information.getMetaData().getLastRefreshed());
        try {
            setLocaleDateTimeFormat(lastRefreshed, DTF);
        } catch (IllegalArgumentException ex) {
            Log.e(TAG, ex.getMessage());
            setLocaleDateTimeFormat(lastRefreshed, DF);
        }

        if (information.getTimeSeriesIntervalMap() != null) {
            Map<String, DataInfo> changeInformationMap;
            try {
                changeInformationMap = getPeriodSeries(information.getTimeSeriesIntervalMap(),
                        localDate, DF);
            } catch (IllegalArgumentException ex) {
                Log.e(TAG, ex.getMessage());
                changeInformationMap = getPeriodSeries(information.getTimeSeriesIntervalMap(),
                        localDate, DTF);
            }
            if (changeInformationMap != null) {
                view.progressBarVisibility(isPeriod, View.GONE);
                if (isPeriod) {
                    calculateChange(changeInformationMap);
                } else {
                    showOptionChange(changeInformationMap);
                }
            }
        } else {
            nullObject();
        }

    }

    interface GetValueListener {

        String getValue(DataInfo dataInfo);

    }
}
