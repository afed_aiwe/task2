package com.task.angelina.networkapplication.utils;

public enum PeriodOfChanges {
    DAY(0),
    MONTH(1),
    HALF_YEAR(2),
    YEAR(3);

    private int position;

    PeriodOfChanges(int i) {
        position = i;
    }
}
