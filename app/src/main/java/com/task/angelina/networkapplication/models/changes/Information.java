package com.task.angelina.networkapplication.models.changes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Map;

public class Information {
    @SerializedName("Meta Data")
    @Expose
    private MetaData metaData;
    @SerializedName(value = "Time Series (60min)", alternate = {"Weekly Time Series", "Monthly Time Series"})
    @Expose
    private Map<String, DataInfo> timeSeriesIntervalMap;
    @SerializedName("Error Message")
    @Expose
    private String errorMessage;

    public MetaData getMetaData() {
        return metaData;
    }

    public Map<String, DataInfo> getTimeSeriesIntervalMap() {
        return timeSeriesIntervalMap;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
