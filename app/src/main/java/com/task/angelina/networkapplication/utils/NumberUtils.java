package com.task.angelina.networkapplication.utils;

public class NumberUtils {

    public static Double stringToDouble(String value) {
        try {
            return Double.valueOf(value);
        } catch (NumberFormatException ex) {
            return 0.0;
        }
    }

    public static Float stringToFloat(String value) {
        try {
            return Float.valueOf(value);
        } catch (NumberFormatException ex) {
            return 0F;
        }
    }
}
