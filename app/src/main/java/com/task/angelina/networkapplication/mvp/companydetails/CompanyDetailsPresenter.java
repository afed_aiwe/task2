package com.task.angelina.networkapplication.mvp.companydetails;

import com.task.angelina.networkapplication.utils.OptionOfChanges;
import com.task.angelina.networkapplication.utils.PeriodOfChanges;

public interface CompanyDetailsPresenter {
    void refresh(String ticker);

    void loadStockPeriodData(String ticker, PeriodOfChanges period);

    void loadStockOptionData(String ticker, PeriodOfChanges period, OptionOfChanges option);

    void backPressed();
}
