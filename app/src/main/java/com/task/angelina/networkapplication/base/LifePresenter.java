package com.task.angelina.networkapplication.base;

public interface LifePresenter {

    void onDestroy();

}
