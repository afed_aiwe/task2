package com.task.angelina.networkapplication.network.service;

import com.task.angelina.networkapplication.models.single.CompanyDetail;
import com.task.angelina.networkapplication.utils.Constants;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SingleCompanyService {
    @GET(Constants.GET_DETAIL_POINT)
    Single<CompanyDetail> getCompany(@Path("ticker") String ticker);
}
