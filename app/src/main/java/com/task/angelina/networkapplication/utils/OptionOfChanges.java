package com.task.angelina.networkapplication.utils;

public enum OptionOfChanges {

    OPEN(0),
    HIGH(1),
    LOW(2),
    CLOSE(3),
    VOLUME(4);

    private int position;

    OptionOfChanges(int i) {
        position = i;
    }
}
