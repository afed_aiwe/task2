package com.task.angelina.networkapplication;

import androidx.fragment.app.Fragment;
import com.task.angelina.networkapplication.ui.fragments.CompanyDetailsFragment;
import com.task.angelina.networkapplication.ui.fragments.CompanyListFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {
    public static final class ListScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return CompanyListFragment.newInstance();
        }
    }

    public static final class DetailsScreen extends SupportAppScreen {
        private final String ticker;

        public DetailsScreen(String ticker) {
            this.ticker = ticker;
        }

        @Override
        public Fragment getFragment() {
            return CompanyDetailsFragment.newInstance(ticker);
        }
    }
}
