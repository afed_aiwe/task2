package com.task.angelina.networkapplication.models.single;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyDetail {
    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("profile")
    @Expose
    private CompanyProfile profile;

    public String getSymbol() {
        return symbol;
    }

    public CompanyProfile getCompanyProfile() {
        return profile;
    }
}
