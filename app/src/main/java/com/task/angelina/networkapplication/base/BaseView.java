package com.task.angelina.networkapplication.base;

import android.app.AlertDialog;

public interface BaseView {

    AlertDialog showAlertDialog(String message);

    void changeRefreshLoadingVisibility(boolean isVisible);
}
