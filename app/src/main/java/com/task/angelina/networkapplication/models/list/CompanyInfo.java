package com.task.angelina.networkapplication.models.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.task.angelina.networkapplication.models.single.CompanyProfile;

public class CompanyInfo {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("ticker")
    @Expose
    private String ticker;
    @SerializedName("name")
    @Expose
    private String name;

    private CompanyProfile companyProfile;

    public CompanyInfo(String id, String ticker, String name) {
        this.id = id;
        this.ticker = ticker;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyProfile getCompanyProfile() {
        return companyProfile;
    }

    public void setCompanyProfile(CompanyProfile companyProfile) {
        this.companyProfile = companyProfile;
    }
}
