package com.task.angelina.networkapplication.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.db.williamchart.view.BarChartView;
import com.squareup.picasso.Picasso;
import com.task.angelina.networkapplication.R;
import com.task.angelina.networkapplication.base.BaseFragment;
import com.task.angelina.networkapplication.common.BackButtonListener;
import com.task.angelina.networkapplication.models.single.CompanyDetail;
import com.task.angelina.networkapplication.models.single.CompanyProfile;
import com.task.angelina.networkapplication.mvp.companydetails.CompanyDetailsPresenterImpl;
import com.task.angelina.networkapplication.mvp.companydetails.CompanyDetailsView;
import com.task.angelina.networkapplication.ui.adapters.companydetails.DetailsPeriodAdapter;
import com.task.angelina.networkapplication.ui.adapters.companydetails.SpinnerAdapter;
import com.task.angelina.networkapplication.utils.OptionOfChanges;
import com.task.angelina.networkapplication.utils.PeriodOfChanges;
import java.util.LinkedHashMap;
import java.util.List;

public class CompanyDetailsFragment extends BaseFragment implements CompanyDetailsView, SwipeRefreshLayout.OnRefreshListener, BackButtonListener {
    private static final String COMPANY_TICKER = "TICKER";
    private String companyTicker;
    private CompanyDetailsPresenterImpl companyDetailsPresenter;
    private ImageView imgLogo;
    private TextView txtCompanyName;
    private TextView txtCompanyDescription;
    private DetailsPeriodAdapter detailsPeriodAdapter;
    private ProgressBar periodProgressBar;
    private ProgressBar optionProgressBar;
    private PeriodOfChanges periodOfChanges;
    private OptionOfChanges optionOfChanges;
    private FrameLayout barChartWrapper;

    public CompanyDetailsFragment() {
    }

    public static CompanyDetailsFragment newInstance(String companyTicker) {
        CompanyDetailsFragment fragment = new CompanyDetailsFragment();
        Bundle args = new Bundle();
        args.putString(COMPANY_TICKER, companyTicker);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            companyTicker = getArguments().getString(COMPANY_TICKER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_company_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imgLogo = view.findViewById(R.id.img_logo);
        txtCompanyName = view.findViewById(R.id.txt_company_name);
        txtCompanyDescription = view.findViewById(R.id.txt_company_description);
        RecyclerView rvPeriods = view.findViewById(R.id.rv_periods);
        Spinner spinnerTime = view.findViewById(R.id.spinner_time);
        Spinner spinnerParam = view.findViewById(R.id.spinner_param);
        refreshLayout = view.findViewById(R.id.swipe_company);
        periodProgressBar = view.findViewById(R.id.progress_period);
        barChartWrapper = view.findViewById(R.id.barChartWrapper);
        optionProgressBar = view.findViewById(R.id.progress_option);

        detailsPeriodAdapter = new DetailsPeriodAdapter();

        rvPeriods.setLayoutManager(new LinearLayoutManager(this.getContext()));
        rvPeriods.setAdapter(detailsPeriodAdapter);

        refreshLayout.setOnRefreshListener(this);
        companyDetailsPresenter.refresh(companyTicker);

        spinnerTime.setSelection(0, false);
        spinnerTime.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                periodOfChanges = PeriodOfChanges.values()[position];
                companyDetailsPresenter.loadStockPeriodData(companyTicker, periodOfChanges);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerAdapter spinnerTimeAdapter = new SpinnerAdapter(this.getContext(), getResources().getStringArray(R.array.period));
        final boolean[] skipFirstItemSelection1 = {true};
        spinnerParam.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (skipFirstItemSelection1[0]) {
                    skipFirstItemSelection1[0] = false;
                    return;
                }

                optionOfChanges = OptionOfChanges.values()[position];
                companyDetailsPresenter.loadStockOptionData(companyTicker, periodOfChanges, optionOfChanges);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerAdapter spinnerParamAdapter = new SpinnerAdapter(this.getContext(), getResources().getStringArray(R.array.options));
        spinnerTime.setAdapter(spinnerTimeAdapter);
        spinnerParam.setAdapter(spinnerParamAdapter);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        companyDetailsPresenter = new CompanyDetailsPresenterImpl();
        companyDetailsPresenter.setView(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        companyDetailsPresenter.onDestroy();
        companyDetailsPresenter = null;
    }

    @Override
    public void companyLoadedSuccess(CompanyDetail companyDetail) {
        CompanyProfile companyProfile = companyDetail.getCompanyProfile();
        if (companyProfile != null) {
            Picasso.with(this.getContext()).load(companyProfile.getImage())
                    .into(imgLogo);
            txtCompanyName.setText(companyProfile.getCompanyName());
            txtCompanyDescription.setText(companyProfile.getDescription());
        } else {
            txtCompanyName.setText(R.string.not_available);
        }
    }

    @Override
    public void companyLoadedError(String error) {
        showAlertDialog(error).show();
    }

    @Override
    public void stockPeriodLoadedSuccess(List<Double> result) {
        detailsPeriodAdapter.setResultArray(result);
        detailsPeriodAdapter.notifyDataSetChanged();
    }

    /**
     *Displays a graph. Due to the lack of a condition for checking graph rendering in the library, it is necessary to recreate the view when choosing another option
     * @param result map that stores points for the graph
     */
    @Override
    public void stockOptionLoadedSuccess(LinkedHashMap<String, Float> result) {
        BarChartView barChartView = (BarChartView) LayoutInflater.from(getContext())
                .inflate(R.layout.chart_layout, barChartWrapper, false);
        barChartWrapper.removeAllViews();
        barChartWrapper.addView(barChartView);
        barChartView.show(result);
    }

    @Override
    public void progressBarVisibility(boolean isPeriod, int visibility) {
        if (isPeriod) {
            periodProgressBar.setVisibility(visibility);
        } else {
            optionProgressBar.setVisibility(visibility);
        }
    }

    @Override
    public void onRefresh() {
        companyDetailsPresenter.refresh(companyTicker);
    }

    @Override
    public boolean onBackPressed() {
        companyDetailsPresenter.backPressed();
        return true;
    }
}
