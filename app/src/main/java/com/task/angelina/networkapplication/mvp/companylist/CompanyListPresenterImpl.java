package com.task.angelina.networkapplication.mvp.companylist;

import android.util.Log;
import com.task.angelina.networkapplication.NetworkApp;
import com.task.angelina.networkapplication.Screens;
import com.task.angelina.networkapplication.base.BasePresenter;
import com.task.angelina.networkapplication.models.list.CompanyInfo;
import com.task.angelina.networkapplication.models.list.CompanyList;
import com.task.angelina.networkapplication.network.RestApi;
import com.task.angelina.networkapplication.network.service.AllCompaniesService;
import com.task.angelina.networkapplication.network.service.SingleCompanyService;
import com.task.angelina.networkapplication.utils.Constants;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import ru.terrakok.cicerone.Router;

public class CompanyListPresenterImpl extends BasePresenter<CompanyListView> implements CompanyListPresenter {
    private static final String TAG = CompanyListPresenterImpl.class.getName();
    private Router router;
    private AllCompaniesService companiesService;
    private SingleCompanyService singleCompanyService;
    private CompositeDisposable compositeDisposable;
    private List<CompanyInfo> companyInfoList;
    private boolean isLoading = false;
    private String nextPage = "";

    public CompanyListPresenterImpl() {
        companiesService = RestApi.createServiceAll(AllCompaniesService.class);
        singleCompanyService = RestApi.createServiceDetail(SingleCompanyService.class);
        compositeDisposable = new CompositeDisposable();
        router = NetworkApp.INSTANCE.getRouter();
    }

    @Override
    public void loadNext() {
        loadPage();
    }

    @Override
    public void refresh() {
        nextPage = "";
        view.changeRefreshLoadingVisibility(true);
        loadPage();
    }

    @Override
    public void openDetailsFragment(String ticker) {
        router.navigateTo(new Screens.DetailsScreen(ticker));
    }

    @Override
    public void onBackPressed() {
        router.exit();
    }

    private void loadPage() {
        if (isLoading || nextPage == null) {
            return;
        }

        isLoading = true;
        compositeDisposable.add(companiesService.getAllCompanies(Constants.API_KEY_LIST, nextPage, Constants.PAGE_SIZE)
                .toObservable()
                .flatMap((Function<CompanyList, ObservableSource<CompanyInfo>>) companyList -> {
                    companyInfoList = companyList.getCompanies();
                    nextPage = companyList.getNextPage();
                    return Observable.fromIterable(companyInfoList);
                })
                .flatMap(
                        (Function<CompanyInfo, ObservableSource<CompanyInfo>>) companyInfo -> getCompanyDetailObservable(companyInfo)
                )
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(companyDetailList -> {
                    if (companyDetailList.isEmpty()) {
                        view.onPageLoadedError(Constants.NOT_FOUND_DATA);
                        view.changeRefreshLoadingVisibility(false);
                    } else {
                        view.setHasMoreItems(nextPage != null);
                        isLoading = false;
                        view.changeRefreshLoadingVisibility(false);
                        view.onPageLoadedSuccess(companyInfoList, "".equals(nextPage));
                    }
                }, throwable -> {
                    isLoading = false;
                    view.changeRefreshLoadingVisibility(false);
                    view.onPageLoadedError(throwable.getMessage());
                    Log.e(TAG, throwable.getMessage());
                    throwable.printStackTrace();
                }));
    }

    @Override
    public void onDestroy() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    private Observable<CompanyInfo> getCompanyDetailObservable(final CompanyInfo companyInfo) {
        return singleCompanyService.getCompany(companyInfo.getTicker())
                .toObservable()
                .map(companyDetail -> {
                    companyInfo.setCompanyProfile(companyDetail.getCompanyProfile());
                    return companyInfo;
                })
                .onErrorResumeNext(Observable.empty());
    }
}
