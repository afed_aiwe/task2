package com.task.angelina.networkapplication.ui.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import com.task.angelina.networkapplication.NetworkApp;
import com.task.angelina.networkapplication.R;
import com.task.angelina.networkapplication.Screens;
import com.task.angelina.networkapplication.common.BackButtonListener;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;

public class MainActivity extends AppCompatActivity {
    private Navigator navigator = new SupportAppNavigator(this, R.id.main_frag);
    private Router router;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        router = NetworkApp.INSTANCE.getRouter();
        openFirstFragment();
    }

    @Override
    protected void onResumeFragments() {
        super.onResume();
        NetworkApp.INSTANCE.getNavigatorHolder().setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        NetworkApp.INSTANCE.getNavigatorHolder().removeNavigator();
        super.onPause();
    }

    private void openFirstFragment() {
        router.newRootScreen(new Screens.ListScreen());
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_frag);
        if (fragment != null && fragment instanceof BackButtonListener && ((BackButtonListener) fragment).onBackPressed()) {
            return;
        } else {
            super.onBackPressed();
        }
    }
}
