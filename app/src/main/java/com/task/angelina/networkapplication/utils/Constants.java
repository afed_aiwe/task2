package com.task.angelina.networkapplication.utils;

public final class Constants {

    public static final String NOT_FOUND_DATA = "NOT_FOUND_DATA";
    public static final String ALL_COMPANIES_URL = "https://api-v2.intrinio.com";
    public static final String GET_COMPANIES_POINT = "companies";
    public static final String DETAIL_COMPANY_URL = "https://financialmodelingprep.com";
    public static final String GET_DETAIL_POINT = "api/v3/company/profile/{ticker}";
    public static final String API_KEY_LIST = "OjkyYmU3NzI1ZjA3NTg0NGRjNjE3YzA5ZTgwN2IwZWZl";
    public static final String HEADER_API_LIST_PARAM = "api_key";
    public static final Integer PAGE_SIZE = 20;
    public static final String PAGE_SIZE_PARAM = "page_size";
    public static final String NEXT_PAGE_PARAM = "next_page";

    public static final String QUERY_PARAM = "query";
    public static final String API_KEY_CHANGE = "1M0ILJ2KQ08IFMK2";
    public static final String HEADER_API_CHANGE_PARAM = "apikey";
    public static final String FULL_SIZE = "full";
    public static final String OUT_PUT_SIZE = "outputsize";
    public static final String FUNCTION_PARAM = "function";
    public static final String SYMBOL = "symbol";
    public static final String FUNCTION_INTRADAY = "TIME_SERIES_INTRADAY";
    public static final String FUNCTION_WEEKLY = "TIME_SERIES_WEEKLY";
    public static final String FUNCTION_MONTH = "TIME_SERIES_MONTHLY";
    public static final String INTERVAL_HEADER = "interval";
    public static final String INTERVAL_PARAM = "60min";
    public static final String CHANGE_COMPANY_URL = "https://www.alphavantage.co";

    private Constants() { }
}
